#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals


# A. Пончики
# Дано количество пончиков (целое число);
# Нужно вернуть строку следующего вида:
# 'Количество пончиков: <count>', где <count> это количество,
# переданное в функцию как параметр.
# Однако, если количество 10 и более - нужно использовать слово
# 'много', вместо текущего количества.
# Таким образом, donuts(5) вернет 'Количество пончиков: 5'
# а donuts(23) - 'Количество пончиков: много'
def donuts(count):
        if count <= 10:
            print 'Number of donuts:', count
        else:
            print 'Number of donuts: lots of'
        return
donuts(10)
donuts(11)

# B. Оба конца
# Дана строка s.
# Верните строку, состоящую из первых 2
# и последних 2 символов исходной строки.
# Таким образом, из строки 'spring' получится 'spng'.
# Однако, если длина строки меньше, чем 2 -
# верните просто пустую строчку.
def both_ends(s):
    if len(s) < 2:
        print ('less then 2 char')
    else:
        print (s[0:2] + s[-2:])

both_ends('lesschar')

# C. Кроме первого
# Дана строка s.
# Верните строку, в которой все вхождения ее первого символа
# заменены на '*', за исключением самого этого первого символа.
# Т.е., из 'babble' получится 'ba**le'.
# Предполагается, что длина строки 1 и более.
# Подсказка: s.replace(stra, strb) вернет версию строки,
# в которой все вхождения stra будут заменены на strb.
def fix_start(s):
    a = s.lstrip(s[:1])
    b = s[0]
    c = a.replace(b, '*')
    print b + c

fix_start('coca-cola')


# D. Перемешивание
# Даны строки a и b.
# Верните одну строку, в которой a и b отделены пробелом '<a> <b>',
# и поменяйте местами первые 2 символа каждой строки.
# Т.е.:
#   'mix', 'pod' -> 'pox mid'
#   'dog', 'dinner' -> 'dig donner'
# Предполагается, что строки a и b имеют длину 2 и более символов.
def mix_up(a, b):
    s = a + ' ' + b
    first = a.lstrip(a[:1])
    second = b.lstrip(b[:1])
    z = a[0]
    x = b[0]
    print x + first + ' ' + z + second

mix_up('faka', 'makafo')
mix_up('mama', 'ama')

# E. Хорош
# Дана строка.
# Найдите первое вхождение подстрок 'не' и 'плох'.
# Если 'плох' идет после 'не' - замените всю подстроку
# 'не'...'плох' на 'хорош'.
# Верните получившуюся строку
# Т.о., 'Этот ужин не так уж плох!' вернет:
# Этот ужин хорош!
def not_bad(s):
    a = s.find('не')
    b = s.rfind('плох')
    if a != -1 and b != -1 and a > b:
        print 'Этот ужин не так уж плох!'
    elif b > a:
        print 'Этот ужин хорош!'
    else:
        print 'Not correct'


not_bad('Этот фильм не так уж плох')
not_bad('А ужин был не плох!')
not_bad("Этот плох, но не совсем")


# F. Две половины
# Рассмотрим разделение строки на две половины.
# Если длина четная - обе половины имеют одинаковую длину.
# Если длина нечетная — дополнительный символ присоединяется к первой половине.
# Т.е., 'abcde', первая половина 'abc', вторая - 'de'.
# Даны 2 строки, a и b, верните строку вида:
# 1-половина-a + 1-половина-b + 2-половина-a + 2-половина-b
def front_back(a, b):
    la = len(a) / 2 + len(a) % 2
    lb = len(b) / 2 + len(b) % 2
    c = a[:la] + b[:lb] + a[la:] + b[lb:]
    print c
front_back('abcd', 'xy')
front_back('abcde', 'xyz')
front_back('Kitten', 'Donut')
